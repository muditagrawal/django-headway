from django.shortcuts import render
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from django.http import HttpResponseRedirect
from django import forms
from .forms import UserRegistrationForm
from django.views import View


class SignUp(View):
    form = UserRegistrationForm()
    
    def get(self, request):
        form = self.form
        return render(request, 'registration/signup.html', {'form': form})
    
    def post(self, request):
        form = UserRegistrationForm(request.POST)
        if form.is_valid():
            userObj = form.cleaned_data
            first_name = userObj['first_name']
            last_name = userObj['last_name']
            username = userObj['username']
            email = userObj['email']
            mobile = userObj['mobile']
            password = userObj['password']
            if not (User.objects.filter(email=email).exists()) or User.objects.filter(username=username).exists():
                User.objects.create_user(username, email, password)
                user = authenticate(email=email, password=password)
                login(request, user)
                return HttpResponseRedirect('/')
            else:
                raise forms.ValidationError('Looks like a email with that email or mobile already exists')
        else:
            form = UserRegistrationForm()