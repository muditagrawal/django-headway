from django import forms


class UserRegistrationForm(forms.Form):
    username = forms.CharField(
        required = True,
        label = 'User name',
        # max_length = 32
    )
    first_name = forms.CharField(
        required = True,
        label = 'First name',
        # max_length = 32
    )
    last_name = forms.CharField(
        required = True,
        label = 'Last name',
        # max_length = 32
    )
    email = forms.CharField(
        required = True,
        label = 'Email',
        # max_length = 32,
    )
    mobile = forms.IntegerField(
        required= True,
        label = 'Mobile',
        # max_length = 10
    )
    password = forms.CharField(
        required = True,
        label = 'Password',
        # max_length = 32,
        widget = forms.PasswordInput()
    )
