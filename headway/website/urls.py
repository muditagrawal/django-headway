from django.conf.urls import url, include
from django.contrib import admin
from .views import SignUp

urlpatterns = [
    url(r'signup/', SignUp.as_view()),
]